// blog.js

const mongoose = require('mongoose');
const Schema = mongoose.Schema;

// Define collection and schema for blog
let registrationModel = new Schema({
  rollNo:{
    type:String,
  },
  firstname: {
    type:String
  },
  lastname: {
    type:String
  },
  email:{
    type:String
  },
  password:{
    type:String
  },
  mobile:{
    type:Number
  },
  gender:{
    type:String
  }
},{
    collection: 'registration'
});

module.exports = mongoose.model('registration', registrationModel);
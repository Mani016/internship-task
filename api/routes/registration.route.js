// blog .route.js
const express = require('express');
const app = express();

const registrationRoutes = express.Router();
let registrationModel = require('../models/registrationmodel');

let loginModel = require('../models/loginmodel');


registrationRoutes.route('/register').post(function (req, res) {
  let registration = new registrationModel(req.body);
  registration.save()
    .then(registration => {
      res.status(200).json({ 'registration': 'registration has been successfully done' });
    })
    .catch(err => {
      res.status(400).send("unable to register");
    });
});

registrationRoutes.route('/login').post(function (req, res) {
  loginModel.find({
    rollNo: req.body.rollNo, password: req.body.password
  }, function (err, user) {
    if (err) throw err;
    if (user.length) {
      return res.status(200).json({
        status: 'success',
        data: user
      })
    } else {
      return res.status(200).json({
        status: 'fail',
        message: 'Login Failed',
      })
    }

  })
});

registrationRoutes.route('/login/create').post(function (req, res) {

  let login = new loginModel(req.body);
  login.save()
    .then(login => {
      res.status(200).json({ 'login': 'login has been created ' });
    })
    .catch(err => {
      res.status(400).send("unable to create login");
    });
});

registrationRoutes.route('/').get(function (req, res) {
  registrationModel.find(function (err, data) {
    if (err) {
      console.log(err);
    }
    else {
      res.json(data);
    }
  });
});

// Defined edit route
registrationRoutes.route('/edit/:id').get(function (req, res) {
  let id = req.params.id;
  registrationModel.find({ "rollNo": id }, function (err, profile) {
    return res.status(200).json({
      status: 'success',
      data: profile
    })
  });
});



//  Defined update route
//  Defined update route
registrationRoutes.route('/update/:id').put(function (req, res) {
  let id = req.params.id;
  registrationModel.updateOne({"rollNo": id },{ $set:{"firstname":req.body.firstName,"lastname":req.body.lastName,"gender":req.body.gender,
  "mobile":req.body.mobile,"email":req.body.email,"rollNo":req.body.rollNo,"password":req.body.password}},{new: true}, function(err, doc) {
    if (err) return res.status(500, {error: err});
    return res.send('Succesfully saved.');
  });
});

// Defined delete | remove | destroy route
registrationRoutes.route('/delete/:id').get(function (req, res) {
  let id = req.params.id;
  registrationModel.deleteOne({ "rollNo": id }, function (err, profile) {
    if (err) res.json(err);
    else res.json('Successfully removed');
  });
});

module.exports = registrationRoutes;
import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';

@Injectable(
)

export class RegistrationService {
    uri = 'http://localhost:4000/api';
    constructor(private http: HttpClient) { }   
    registration(obj):Observable<any> {
        let param = {
            rollNo:obj['rollNo'],
            firstname: obj['firstName'],
              lastname:obj['lastName'],
              gender:obj['gender'],
              email:obj['email'],
              password:obj['password'],
              mobile:obj['mobile'],
        };
        this.login(obj).subscribe();
        return this.http.post(`${this.uri}/register`, param);
      }

      login(obj):Observable<any>{
        let param = {
          rollNo:obj['rollNo'],
          password:obj['password']
        }
        return this.http.post(`${this.uri}/login/create`, param);
      }

      getProfileById(rollNo):Observable<any>{
        return this.http.get(`${this.uri}/edit/${rollNo}`)
      }

      updateProfile(data,rollNo):Observable<any>{
        console.log(data)
        return this.http.put(`${this.uri}/update/${rollNo}`,data)
      }

      deleteProfile(rollNo):Observable<any>{
      return this.http.get(`${this.uri}/delete/${rollNo}`)
      }

    }

    
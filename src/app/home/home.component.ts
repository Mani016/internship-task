import { Component, OnInit } from '@angular/core';
import { RegistrationService } from 'src/services/register/register.service';
import { ActivatedRoute, Router } from '@angular/router';
import Swal from 'sweetalert2';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit {
  rollNo:any;
  name:any;
  email:any;

  constructor( private registrationService:RegistrationService,private activatedRoute: ActivatedRoute,private router:Router) {
    
   }

  ngOnInit(): void {
    this.rollNo = this.activatedRoute.snapshot.paramMap.get('rollNo');   
    this.registrationService.getProfileById(this.rollNo).subscribe(data=>{
      this.name = data.data[0].firstname;
      this.email = data.data[0].email;
    })
  }

  editProfile(){
    this.router.navigate([`/${this.rollNo}/edit/registration`]);
  }

  deleteProfile(){
    Swal.fire({
      title: 'Are you sure?',
      text: "You won't be able to revert this!",
      icon: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Yes, delete it!'
    }).then((result) => {
      if (result.value) {
        this.registrationService.deleteProfile(this.rollNo).subscribe(
          data=>{
            Swal.fire(
              'Deleted!',
              'Your file has been deleted.',
              'success'
            )
            this.router.navigate(['/registration'])
          }
        )
       
      }
    })

  }

}

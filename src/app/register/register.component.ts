import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators, FormControl } from '@angular/forms';
import { Router, ActivatedRoute } from '@angular/router';
import { RegistrationService } from 'src/services/register/register.service';
import Swal from 'sweetalert2';
@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.scss']
})
export class RegisterComponent implements OnInit {
  registrationForm: FormGroup;
  rollNo: any;
  pageType:any;

  constructor(private _formBuilder: FormBuilder,
    private router: Router,
    private _registrationService: RegistrationService,
    private activatedRoute: ActivatedRoute

  ) { }

  ngOnInit(): void {
    this.rollNo = this.activatedRoute.snapshot.paramMap.get('rollNo');   

    this.pageType = this.activatedRoute.snapshot.paramMap.get('handle');   
    this.pageType === 'edit' ? this.getProfileByRollNo() : '';

    this.registrationForm = this._formBuilder.group({
      rollNo: ['', [Validators.required]],
      firstName: ['', [Validators.required]],
      lastName: ['', [Validators.required]],
      email: new FormControl(null, [Validators.required, Validators.pattern(/^[a-zA-Z0-9.!#$%&'*+/=?^_`{|}~-]+@[a-zA-Z0-9-]+(?:\.[a-zA-Z0-9-]+)*$/)]),
      mobile: new FormControl(null, [Validators.required, Validators.pattern(/[6789]{1}[0-9]{9}/)]),
      password: ['', [Validators.required]],
      gender: ['', [Validators.required]],
    });

  }
  submitform(){
      this._registrationService.registration(this.registrationForm.value).subscribe(data=>{
        Swal.fire({
          icon: 'success',
          title: 'You are successfully registered',
          showClass: {
            popup: 'animated fadeInDown faster'
          },
          hideClass: {
            popup: 'animated fadeOutUp faster'
          },
          allowOutsideClick: false,
          confirmButtonColor: '#3085d6',
          confirmButtonText: 'OK!',
    
        }).then((result) => {
          if (result.value) {
            this.router.navigate(['/login']);
          }
        });
          })
    }  
    getProfileByRollNo(){
      this._registrationService.getProfileById(this.rollNo).subscribe(
        data=>{
          console.log(data);  
                this.registrationForm.patchValue({
                  rollNo: this.rollNo,
                  firstName: data.data[0].firstname,
                  lastName: data.data[0].lastname,
                  email:data.data[0].email,
                  mobile:data.data[0].mobile,
                  password:data.data[0].password,
                  gender:data.data[0].gender
                })
        }
      )

    }
    
    updateForm(){
      this._registrationService.updateProfile(this.registrationForm.value,this.rollNo).subscribe(
        data=>{
          console.log(data);
          
        } 

      )
      Swal.fire({
        title: 'Updated Successfully',
        icon: 'success',
        showCancelButton: false,
        confirmButtonColor: '#3085d6',
        confirmButtonText: 'Ok!'
      }).then((result) => {
        if (result.value) {
          this.router.navigate([`/${this.rollNo}/home`])
        } 
      })

    }
    back(){
      this.router.navigate([`/${this.rollNo}/home`])
    }

}

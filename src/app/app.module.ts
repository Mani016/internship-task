import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';

import { AppComponent } from './app.component';
import { LoginComponent } from './login/login.component';
import { RegisterComponent } from './register/register.component';
import { HomeComponent } from './home/home.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { RegistrationService } from 'src/services/register/register.service';
import { HttpClientModule } from '@angular/common/http';
import { LoginService } from 'src/services/login/login.service';
const routes =[
  {
    path: '',
    component:RegisterComponent
  },
  {
    path:'registration',
    component:RegisterComponent

  },
  {
    path: ':rollNo/:handle/registration',
    component:RegisterComponent
  },
  {
    path: 'login',
    component:LoginComponent
  },
  {
    path: ':rollNo/home',
    component:HomeComponent
  }
]


@NgModule({
  declarations: [
    AppComponent,
    LoginComponent,
    RegisterComponent,
    HomeComponent,
    

  ],
  imports: [
    BrowserModule,
    RouterModule.forRoot(routes),
    FormsModule,
    ReactiveFormsModule,
    HttpClientModule
  ],
  providers: [RegistrationService,LoginService],
  bootstrap: [AppComponent]
})
export class AppModule { } 

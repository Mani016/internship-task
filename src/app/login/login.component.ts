import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators, FormControl } from '@angular/forms';
import { Router } from '@angular/router';
import { LoginService } from 'src/services/login/login.service';
import Swal from 'sweetalert2';
@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {
  loginForm: FormGroup;

  constructor(private _formBuilder: FormBuilder,
    private router: Router,
    private _loginService: LoginService) { 
            
    this.loginForm = this._formBuilder.group({
      rollNo: ['', [Validators.required]],     
      password: ['', [Validators.required]],
    });
  }

  ngOnInit(): void {
  }
  submitform(){
       this._loginService.login(this.loginForm.value).subscribe(data => {
      if (data.status === "success") {
        this.router.navigate([`/${this.loginForm.value.rollNo}/home`])
      }
      else {
        Swal.fire({
          icon: 'warning',
          title: 'Incorrect Username or Password',
          showClass: {
            popup: 'animated fadeInDown faster'
          },
          hideClass: {
            popup: 'animated fadeOutUp faster'
          },
          allowOutsideClick: false,
          confirmButtonColor: '#3085d6',
          confirmButtonText: 'OK!',

        })
      }

    })

  }

}
